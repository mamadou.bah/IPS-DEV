#include <iostream>
#include <armadillo>

int     ft_factoriel(int num)
{
        if (num == 1)
        {
            return (1);
        }
        else
        {
            return (num * ft_factoriel(num - 1));
        }
}

int main(int argc, char* argv[])
{
    int n = atoi(argv[1]);
    printf("%d",n);

    arma::vec Z = arma::linspace<arma::vec>(-10, 10);    //par défaut le vecteur aura 100 valeurs

    /*double k;
    Z.print("vecteur z");
    k=Z(10);
    std::cout << k;*/

    arma::mat H = arma::mat(100,1,arma::fill::ones);// on crée la matrice, remplie de 1 au début avec 1 colonnes

    //H.print("matrice H");

    H.insert_cols(1, 2*Z);

    //H.print("matrice H");

    /*H.col(1).print("colonne1");
    H.insert_cols(2, 2*H.col(1)-(2*(1)*H.col(0)));
    H.col(2).print("colonne2");
    H.print("matrice H");*/
    int i;

    for(i=2; i<n; i++){

    arma::vec A = 2*Z%H.col(i-1)-(2*(i-1)*H.col(i-2));
    //A.print("A");
    H.insert_cols(i, A);

    }

    H.print("matrice H");

    arma::vec Z2 = arma::exp(-0.5*(arma::pow(Z, 2)));

    Z2.print("Z2");

    int j;

    //for (j=0; j<n; j++){


    arma::vec F = (1/sqrt((pow(2, n))*ft_factoriel(n)))*((pow((1/3.14), 0.25)))*Z2%H.col(n-1);

    //}

    F.print("vecteur F(n)");

    return 0;
}
