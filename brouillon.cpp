#include <iostream>
#include <armadillo>

int main()
{
    arma::vec Z = arma::linspace<arma::vec>(-10, 10);    //par défaut le vecteur aura 100 valeurs


    //Z.print("vecteur z");

    arma::mat H = arma::mat(100,1,arma::fill::ones);// on crée la matrice, remplie de 1 au début avec 10 colonnes

    H.print("matrice H");

    H.insert_cols(1, 2*Z);

    H.print("matrice H");

    /*H.col(1).print("colonne1");
    H.insert_cols(2, 2*H.col(1)-(2*(1)*H.col(0)));
    H.col(2).print("colonne2");
    H.print("matrice H");*/
    int i;

    for(i=2; i<8; i++){

    arma::vec A = 2*H.col(i-1)-(2*(i-1)*H.col(i-2));
    //A.print("A");
    H.insert_cols(i, A);

    }

    H.print("matrice H");

    return 0;
}
