#include <fstream>
#include <iostream>
#include <armadillo>
using namespace std;

int     ft_factoriel(int num)
{
        if (num == 1)
        {
            return (1);
        }
        else if (num == 0)
        {
            return(1);
        }
        else
        {
            return (num * ft_factoriel(num - 1));
        }
}

int main(int argc, char* argv[])
{
  int i,j, n = atoi(argv[1]);

    arma::vec Z = arma::linspace<arma::vec>(-10, 10);    //par défaut le vecteur aura 100 valeurs

    arma::mat H = arma::mat(100,1,arma::fill::ones);// on crée la matrice, remplie de 1 au début avec 1 colonnes

    arma::mat Xi = arma::mat(10,1,arma::fill::ones);

    arma::mat Wi = arma::mat(10,1,arma::fill::ones);

    arma::mat HO = arma::mat(10,1,arma::fill::ones);

    arma::mat ORTO = arma::mat(10,10,arma::fill::zeros);
    
    
    ifstream fichier("gauss.txt", ios::in);
    
     if(fichier) 
        {      		     
	  for (i=0;i<10;i++){
	    fichier>>Xi(i,0)>>Wi(i,0); 
	    	  }      
          fichier.close();	       
        }

       else 
	  {
	    cerr << "Erreur à l'ouverture !" << endl;}

    H.insert_cols(1, 2*Z);
    HO.insert_cols(1, 2*Xi);
   

    for(i=2; i<n; i++){

        arma::vec A = 2*Z%H.col(i-1)-(2*(i-1)*H.col(i-2));
        H.insert_cols(i, A);
       	arma::vec B = 2*Xi%HO.col(i-1)-(2*(i-1)*HO.col(i-2));
	 HO.insert_cols(i, B);   
        }
   
     for(i=0;i<10;i++){
      for (j=i;j<10;j++){
    	ORTO(i,j)=arma::accu(HO.col(i)%HO.col(j)%Wi);
        }      
    } 

    H.print("matrice H");

    arma::vec Z2 = arma::exp(-0.5*(arma::pow(Z, 2)));
    arma::mat J = arma::mat(100,1);
       //je crée une matrice colonne. Dans J il y aura donc les F(j) pour j allant de 0 à n (passé en argument)

    for (j=1; j<n; j++){
        arma::vec F = (1/sqrt((pow(2, j))*ft_factoriel(j)))*((pow((1/3.14), 0.25)))*Z2%H.col(j);
        F.print("F");
        J.insert_cols(j, F);
        }
    
    J.print("vecteurs F(n)");
    
 //attention la derniere colonne ne correspond à rien c est juste les valeurs mises par defaut lors de l'initialisation de J

ofstream fichier1("test.txt", ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
        
 if(fichier1) 
        {     		     
	  fichier1<<J;
          fichier1.close();	 	       
        }
        else 
	  {
	    cerr << "Erreur à l'ouverture !" << endl;}
 
ofstream fichier2("Orto.txt", ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
       
 if(fichier2) 
        {      		     
	  fichier2<<ORTO;       
          fichier2.close();	 	       
        }
        else 
	  {
	    cerr << "Erreur à l'ouverture !" << endl;}
 


 Xi.print("valeurs Xi");
 Wi.print("valeurs Wi");
 ORTO.print("ortogonalite");

      
    return 0;
}
